# Nodejs Style Guide
-------------------------------------
## 1. Format
### 1.1 Sử dụng 2 space
### 1.2 Newlines
 - ```sử dụng UNIX-style newlines (\n) ```

### 1.3 Không có khoảng trống ở cuối(No trailing whitespace)

### 1.4 Sử dụng dấu chấm phẩy(Semicolons)
### 1.5 80 ký tự trên 1 dòng
### 1.6 Sử dụng dấu nháy đơn (ngoại trừ viết JSON)

#### bad:

  - ``` let name = "quoctv" ```

#### good:

  - ``` let name = 'quoctv' ```

### 1.7 Sử dụng whitespace sau keywords
#### bad:

  - ``` if(condition) { ... } ```

#### good:

  - ``` if (condition) { ... } ```

### 1.8 Sử dụng whitespace sau tên function
#### bad:

  - ```sh
     function name(arg) { ... }
     run(function() { ... })  
    ```

#### good:

  - ```sh
     function name (arg) { ... }
     run(function () { ... }) 
    ```

### 1.9 Dấu ngoặc nhọn (Curly braces)
  - Giữ các câu lệnh khác trên cùng một dòng với dấu ngoặc nhọn.
#### bad:

```sh
  if (true)
  {
    console.log('bad');
  }
  -------------------------------------

  if (condition) {
    // ...
  }
  else {
    // ...
  }
```
#### good:

```sh
  if (true) {
    console.log('good');
  } 
  -------------------------------------
  if (condition) {
    // ...
  } else {
    // ...
  }
```
### 1.10 Khai báo mỗi biến trên 1 câu lệnh
#### bad:

  
```sh
  let keys = ['foo', 'bar'],
      values = [23, 42],
      object = {},
      key;

  while (keys.length) {
    key = keys.pop();
    object[key] = values.pop();
  }  
```

#### good:

```sh
  let keys   = ['foo', 'bar'];
  let values = [23, 42];
  let object = {};

  while (keys.length) {
    let key = keys.pop();
    object[key] = values.pop();
  }
```
- Khai báo một biến cho mỗi câu lệnh , nó giúp dễ dàng sắp xếp lại các dòng.

--------------------------------------------------------------------------

## 2. Đặt tên hàm và biến (Naming Conventions)
### 2.1 Sử dụng UPPERCASE(chữ in hoa) cho hằng số
  - Các hằng số phải được khai báo là các biến thông thường hoặc các thuộc tính lớp tĩnh, sử dụng tất cả các chữ hoa.

#### bad:

  ``` const hours = 24; 
  const daysInMonth = 30;
```

#### good:

  ``` const HOURS = 24; 
  const DAYS_IN_MONTH = 30;
```
### 2.2 Sử dụng UCC(UpperCamelCase) cho class name

#### bad:

  ```sh
  class mailService() {
  }
 ```

#### good:

  ```sh 
  class MailService() {
  } 
  ```
### 2.3 Sử dụng LCC(LowerCamelCase) cho biến (Variables), thuộc tính(properties) và tên hàm(function)
 - Các biến, thuộc tính và tên hàm nên sử dụng lowerCamelCase. 
 - Chúng cũng nên mang tính mô tả. 
 - Các biến ký tự đơn và các từ viết tắt không phổ biến nói chung nên tránh.

#### bad:

  ``` let total_item; ```

#### good:

  ``` let totalItem; ```

--------------------------------------------------------------------------

## 3. Điều kiện (Conditionals)
### 3.1 Sử dụng toán tử === và !==.
  - Luôn sử dụng toán tử === thay cho == .
  - Lưu ý: đối với object có thể dùng toán tử == để kiểm tra cho null hoặc undefined.

#### bad:

  ```sh
  if (name != 'quoctv')
  if (name == 'quoctv')
  ```

#### good:

  ```sh
  if (name !== 'quoctv')
  if (name === 'quoctv')
  ```
### 3.2 Kiểm tra với nhiều điều kiện.

#### bad:

  ```sh
    if (password.length >= 6 && /^(?=.*\d).{6,}$/.test(password)) {
      console.log('bad');
    }
  ```

#### good:

  ```sh
    let isValidPassword = password.length >= 6 && /^(?=.*\d).{6,}$/.test(password);

    if (isValidPassword) {
      console.log('good');
    }
  ```
### 3.3 Tránh điều kiện Yoda(https://en.wikipedia.org/wiki/Yoda_conditions)
```sh
  if (25 === age) { }    // ✗ avoid
  if (age === 25) { }    // ✓ ok
```
--------------------------------------------------------------------------

## 4. Biến (variables)
### 4.1 Khai báo biến object hoặc array.
  - Sử dụng dấu phẩy và đặt các khai báo ngắn trên một dòng .

#### bad:

  ```sh
  var a = [
    'hello', 'world'
  ];
  var b = {"good": 'code'
          , is generally: 'pretty'
          };
  ```

#### good:

  ```sh
  var a = ['hello', 'world'];
  var b = {
    good: 'code',
    'is generally': 'pretty',
  };
  ```
### 4.2 Khai báo biến với hằng số.
  - Lí do: Thuận lợi cho việc tìm kiếm và sửa đổi sau này.

#### bad:

  ```sh
    setTimeout (function() {
      client.connect(afterConnect);
    }, 1000);

    function afterConnect() {
      console.log('good');
    }
  ```

#### good:

  ```sh
    const TIME_CONNECT= 1000;
    setTimeout (function() {
      client.connect(afterConnect);
    }, TIME_CONNECT);

    function afterConnect() {
      console.log('good');
    }
  ```
--------------------------------------------------------------------------

## 5. Hàm (Functions)
### 5.1 Không sử dụng nested(lồng) closures function.
  - Lí do: function rõ ràng dễ đọc hơn.

#### bad:

  ```sh
  setTimeout (function() {
    client.connect(function() {
      console.log('bad');
    });
  }, 1000);
  ```

#### good:

  ```sh
  setTimeout (function() {
    client.connect(afterConnect);
  }, 1000);

  function afterConnect() {
    console.log('good');
  }
  ```
### 5.2 Tránh lồng câu lệnh điều kiện trong function.
  - Lí do: 
    - function rõ ràng dễ đọc hơn.
    - Kết quả xử lý trả về nhanh hơn.

#### bad:

  ```sh
  function isPercentage (val) {
    if (val >= 0) {
      if (val < 100) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  ```

#### good:

  ```sh
  function isPercentage (val) {
    if (val < 0) {
      return false;
    }

    if (val > 100) {
      return false;
    }

    return true;
  }
  ```
### 5.3 Luôn xử lý tham số hàm err.
#### bad:
  ```sh
  run(function (err) {
    console.log('done')
  })
  ```

#### good:
  ```sh
  run(function (err) {
    if (err) throw err
    console.log('done')
  })
  ```
### 5.4 Phải viết arrow function khi sử dụng ES6.
#### 5.4.1 Trong trường hợp nhiều tham số
#### bad:
  ```sh
  // ES5 
  var multiply = function(x, y) {
      return x * y;
  }; 
  ```

#### good:
  ```sh
  // ES6 
  var multiply = (x, y) => { return x * y };
  ```
#### good best(đối với trường hợp 1 biểu thức như trên):
  ```sh
  // ES6 
  var multiply = (x, y) => x * y ;
  ```
#### 5.4.2 Trong trường hợp 1 tham số

#### bad:
  ```sh
  //ES5 
  var phraseSplitterEs5 = function phraseSplitter (phrase) { 
      return phrase.split(' '); 
  }; 
  ```

#### good:
  ```sh
  //ES6 
  var phraseSplitterEs6 = phrase => phrase.split(" ");
  ```
#### 5.4.3 Trong trường hợp không có tham số

#### bad:
  ```sh
  //ES5 
  var docLogEs5 = function docLog() { 
      console.log('bad'); 
  }; 
  ```

#### good:
  ```sh
  //ES6 
  var docLogEs6 = () => { 
    console.log('good'); 
  } 
  ```
--------------------------------------------------------------------------

## 6. Chú Thích (Comment)
  - Sử dụng dấu // để chú thích.
  - 
#### bad:

  ```sh

  ```

#### good:

  ```sh

  ```